/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>
/*==================[macros and definitions]=================================*/
uint8_t vector_bcd[4];

typedef struct
{
	uint8_t port;				/*!< GPIO port number */
	uint8_t pin;				/*!< GPIO pin number */
	uint8_t dir;				/*!< GPIO direction ‘0’ IN;  ‘1’ OUT */
} gpioConf_t;

uint8_t mascara=(1<<0);
uint8_t mascara_ceros=0;

gpioConf_t placa[4]={{1,4,1},{1,5,1},{1,6,1},{2,14,1}};

/*==================[internal functions declaration]=========================*/
void Display_7seg(uint8_t bcd,  gpioConf_t vec[4] ){
	int i;
	for(i=0;i<4;i++){
		if (bcd & mascara){
			printf(" Prende puerto %d \t",vec[i].port);
			printf("Pin: %d \n", vec[i].pin);

		}
		else{
			printf(" NO Prende puerto %d \t",vec[i].port);

			printf("Pin: %d \n", vec[i].pin);

		}
		mascara=mascara << 1;
	}

	mascara=(1<<0);

}
void  BinaryToBcd (uint32_t data, uint8_t digits, uint8_t * bcd_number )
{

	uint32_t aux=10;
	uint8_t i;
	for(i=0;i<digits;i++){

		bcd_number[i]=data%aux;
		data=data/aux;


	}

}

int main(void)
{

/*int i;
for(i=0;i<4;i++){
	printf("Puerto %d \n",placa[i].port);

	printf("Pin %d \n",placa[i].pin);
	printf("Entrada(0), Salida(1)\n");
	printf("%d \n",placa[i].dir);
	printf(" \n");
}*/
uint32_t num=1234;
uint8_t dig=4;

int i;

BinaryToBcd(num,dig,vector_bcd);

for(i=0;i<dig;i++){
	printf("Numero: %u \n",vector_bcd[i]);
	Display_7seg(vector_bcd[i], placa);
	printf("\n");
}




return 0;
}

/*==================[end of file]============================================*/

