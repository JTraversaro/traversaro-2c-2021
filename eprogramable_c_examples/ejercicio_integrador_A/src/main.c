/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>

/*==================[macros and definitions]=================================*/

typedef enum{ON,OFF,TOOGLE}modo_t;
typedef enum{Led_1=1,Led_2,Led_3}led_t;

typedef struct {
	led_t n_led;       // indica el número de led a controlar
	uint8_t n_ciclos;   //  indica la cantidad de ciclos de encendido/apagado
	uint8_t periodo;   //   indica el tiempo de cada ciclo
	modo_t mode;     //    ON, OFF, TOGGLE
} leds;



uint8_t i;
/*==================[internal functions declaration]=========================*/
void Estados_Leds(leds puntero_leds){

	switch(puntero_leds->mode){
	case ON:

		switch(puntero_leds->n_led){
		case Led_1:
			printf("Prende led %u",puntero_leds->n_led);
			break;
		case Led_2:
			printf("Prende led %u",puntero_leds->n_led);
			break;
		case Led_3:
			printf("Prende led %u",puntero_leds->n_led);
			break;

		}
		break;


		case OFF:

			switch(puntero_leds->n_led){
			case Led_1:
				printf("Apaga led %u",puntero_leds->n_led);
				break;
			case Led_2:
				printf("Apaga led %u",puntero_leds->n_led);
				break;
			case Led_3:
				printf("Apaga led %u",puntero_leds->n_led);
				break;

			}


			break;


			case TOOGLE:

				for(i=0;i<puntero_leds->n_ciclos;i++){
					switch(puntero_leds->n_led){
					case Led_1:
						printf("Conmuta led %u",puntero_leds->n_led);
						break;
					case Led_2:
						printf("Conmuta led %u",puntero_leds->n_led);
						break;
					case Led_3:
						printf("Conmuta led %u",puntero_leds->n_led);
						break;

					}

					break;

				}
				break;



	}

}

int main(void)
{	//cargada de datos

	leds led = {Led_1, 3 , 5 , 10 ,OFF};


	Estados_Leds(&led);
	return 0;
}

/*==================[end of file]============================================*/

