/*! @mainpage Ultrasonido
 *
 * \section genDesc General Description
 *
 * Esta aplicacion emplea esl sensor de ultrasonido hc_sr4, funcionando como ejemplo de su implementacion
 *
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	5V			|
 * | 	PIN2	 	| 	Trig		|
 * | 	PIN3	 	| 	Echo		|
 * | 	PIN4	 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 19/10/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Traversaro Julian
 *
 */


#ifndef _ULTRASONIDO_H
#define _ULTRASONIDO_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _ULTRASONIDO_H */

