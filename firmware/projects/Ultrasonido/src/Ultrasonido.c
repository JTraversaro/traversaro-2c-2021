/*! @mainpage Ultrasonido
 *
 * \section genDesc General Description
 *
 * Esta aplicacion emplea esl sensor de ultrasonido hc_sr4, funcionando como ejemplo de su implementacion
 *
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	+5V	 		| 	5V			|
 * | 	T_FIL3	 	| 	Trig		|
 * | 	T_FIL2	 	| 	Echo		|
 * | 	GND		 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 19/10/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Traversaro Julian
 *
 */

/*==================[inclusions]=============================================*/
#include "hc_sr4.h"       /* <= own header */
#include "gpio.h"
#include "systemclock.h"
/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){
	SystemClockInit();
	//Ingreso pines a usar
	HcSr04Init(GPIO_T_FIL2,GPIO_T_FIL3);

	int16_t distancia_Ultrasonido_cm=0;
    while(1){

    	distancia_Ultrasonido_cm=HcSr04ReadDistanceCentimeters();

	}
    
	return 0;
}

/*==================[end of file]============================================*/

