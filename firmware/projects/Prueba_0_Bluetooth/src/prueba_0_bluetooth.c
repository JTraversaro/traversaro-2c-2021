/*
 *
 * Prueba de acelerometro	MMA7260Q
 *
 * @author Traversaro Julian
 *
 * |    MMA7260Q 	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * |   	  PINX	 	| 	   CH1		|
 * |   	  PINY	 	| 	   CH2 		|
 * |   	  PINZ	 	| 	   CH3		|
 * |   	  GS1	 	|    T_FIL0 	|
 * |   	  GS2	 	| 	 T_FIL2		|
 * |   	  VCC	 	| 	  +3.3V		|
 * |   	  GND	 	| 	   GND		|
 * |   	  SM	 	| 	  +3.3V		|
 *
 ** |    HC05	 	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * |   	  PINRX	 	| 	   232_TX	|
 * |   	  PINTX	 	| 	   232_RX	|
 * |   	  VCC	 	| 	  +3.3V		|
 * |   	  GND	 	| 	   GND		|
 *
 *
 */

/*==================[inclusions]=============================================*/
#include "prueba_0_bluetooth.h"       /* <= own header */

#include "gpio.h"
#include "systemclock.h"
#include "delay.h"
#include "switch.h"
#include "led.h"
#include "timer.h"
#include "MMA7260Q.h"

#include "HC05.h"
/*==================[macros and definitions]=================================*/
float eje_x;
float eje_y;
float eje_z;
float promediacion;
#define N 3
#define ROT_0	'0'
#define ROT_90	'1'
#define INTERVALO 100
uint8_t dato_90=ROT_90;
uint8_t dato_0=ROT_0;
/*==================[internal data definition]===============================*/
void Medir();
timer_config my_timer = {TIMER_A,INTERVALO,&Medir};

/*==================[internal functions declaration]=========================*/
float promedio(){
	float prom;
	prom =(eje_x+eje_y+eje_z)/N;
	return prom;
}

void Medir(){
	LedOn(LED_2);
	eje_x=ReadXValue();
	eje_y=ReadYValue();
	eje_z=ReadZValue();

	promediacion=promedio();

	if (promediacion>=0.10*(eje_x+eje_y+eje_z)){
		//				HC05SendString( "Se envia promediacion\r\n");
		//				HC05SendString( "ROT_90\r\n");
		HC05SendByte(&dato_90);

	}

	else{
		HC05SendByte(&dato_0);

		//				HC05SendString( "NO se envia promediacion\r\n");
		//				HC05SendString( "ROT_0\r\n");

	}


}
/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/
/*
 *
 */

void SisInit(){
	hc05_config puerto;
		puerto.baud_rate=115200;
		puerto.rol=RolMaestro;
		puerto.p_serial=NULL;
		uint8_t promediacion;
		int8_t tecla;

		//inits
		SystemClockInit();
		SwitchesInit();
		LedsInit();
		HC05Init(&puerto);
		MMA7260QInit(GPIO_T_FIL0,GPIO_T_FIL2);
		TimerInit(&my_timer);
		TimerStart(TIMER_A);

}
int main(void){

	SisInit();
	while(1){





	return 0;
}

/*==================[end of file]============================================*/

