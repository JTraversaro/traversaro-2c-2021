/*! @mainpage Contador_Cinta
 *
 * \Contador de objetos en cinta transportadora

 *
 * Esta aplicacion cuenta objetos, con un rango maximo de 16 objetos, y enciende en codigo binario los led de la placa
 *
 * \section hardConn Hardware Connection
 *
 * | 	Tcrt5000	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO0		|
 * |	Vcc			|		5 V		|
 * |	GND			|	GND			|
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 13/9/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Traversaro Julian
 */

/*==================[inclusions]=============================================*/
#include "contador_distancia.h"       /* <= own header */
#include "DisplayITS_E0803.h"
#include "gpio.h"
#include "systemclock.h"
#include "Tcrt5000.h"
#include "delay.h"
#include "led.h"
#include "switch.h"
#include "timer.h"
#include "uart.h"
/*==================[macros and definitions]=================================*/

#define TIEMPOMS 200
//#define TIEMPOms 100
#define ON 1
#define OFF 0
#define INTERVALO 100
#define DIAMETRO_RUEDA 32
#define CANTIDAD_APERTURAS 20
#define DISTANCIA_POR_ORIFICIO DIAMETRO_RUEDA/CANTIDAD_APERTURAS

bool TEC1=OFF;
bool TEC2=OFF;
bool TEC3=OFF;
uint8_t teclas;
bool disco_contador=OFF;
int16_t cantidad_obj=0;
uint8_t dato_teclado;
/*==================[internal data definition]===============================*/
void Contar();
void Teclado();
void InformoDistancia();
timer_config my_timer = {TIMER_A, INTERVALO , &Contar};
timer_config my_timer2= {TIMER_B,10*INTERVALO,&InformoDistancia};
serial_config lectura ={SERIAL_PORT_PC,115200,&Teclado};

/*==================[internal functions declaration]=========================*/

bool Tec1(){
	//PRENDO-APAGO
	TEC1=!TEC1;
	return true;
}
bool Tec2(){
	//HOLDEO
	TEC2=!TEC2;
	return true;
}
bool Tec3(){
	//RESET
	TEC3=!TEC3;
	return true;
}

void InformoDistancia(){
	int16_t distancia_medida = cantidad_obj*DISTANCIA_POR_ORIFICIO;
	UartSendString(&lectura, UartItoa(distancia_medida, 10));
	UartSendString(SERIAL_PORT_PC, " cm\n\r");
}
void Contar(){


	if(TEC1){
		//Inicio
		disco_contador	= Tcrt5000State();
		if(disco_contador){
			cantidad_obj=cantidad_obj+1;

		}



		if(TEC2){
			//Apago
			TEC1=OFF;


		}
		if(TEC3){
			//RESETEO
			cantidad_obj=0;
			Tec3();

		}
	}


}

void Teclado(){
	UartReadByte(SERIAL_PORT_PC, &dato_teclado);
	switch(dato_teclado){
	case 'O':
		Tec1();
		break;
	case 'H':
		Tec2();
		break;
	case 'R':
		Tec3();
		break;
	}
}

void SisInit(void)
{
	//Inits
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	Tcrt5000Init(GPIO_T_COL0);
	TimerInit(&my_timer);
	TimerInit(&my_timer2);
	TimerStart(TIMER_A);
	TimerStart(TIMER_B);
	UartInit(&lectura);
	SwitchActivInt(SWITCH_1, Tec1);
	SwitchActivInt(SWITCH_2, Tec2);
	SwitchActivInt(SWITCH_3, Tec3);

}


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){

	SisInit();

	return 0;

}




/*==================[end of file]============================================*/

