/*! @mainpage Template
 *
 * \section genDesc General Description
 *
 * Esta aplicacion cuenta objetos, con un rango maximo de 16 objetos, y enciende en codigo binario los led de la placa
 *
 *
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO0		|
 * | 	PIN2	 	| 	5 V			|
 * | 	PIN3	 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * | 02/01/2020	| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author Traversaro Julian
 *
 */

#ifndef _CONTADOR_CINTA_TIMER_H
#define _CONTADOR_CINTA_TIMER_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _CONTADOR_CINTA_TIMER_H */

