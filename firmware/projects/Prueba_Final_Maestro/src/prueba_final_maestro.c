/*! @mainpage Aplicacion Final Maestra
 *
 * \section genDesc General Description
 *
 *  Aplicacion final Maestra, la cual debe ser colocada en el casco de la persona
 * con la cual controlara la apertura y cierre de la puerta
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
 * |    MMA7260Q 	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * |   	  PINX	 	| 	   CH1		|
 * |   	  PINY	 	| 	   CH2 		|
 * |   	  PINZ	 	| 	   CH3		|
 * |   	  GS1	 	|    T_FIL0 	|
 * |   	  GS2	 	| 	 T_FIL2		|
 * |   	  VCC	 	| 	  +3.3V		|
 * |   	  GND	 	| 	   GND		|
 * |   	  SM	 	| 	  +3.3V		|
 *
 ** |    HC05	 	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * |   	  PINRX	 	| 	   232_TX	|
 * |   	  PINTX	 	| 	   232_RX	|
 * |   	  VCC	 	| 	  +3.3V		|
 * |   	  GND	 	| 	   GND		|
 *
 * @author Traversaro Julian
 *
 * 		juliantraversaro@gmail.com
 *
 *
 */

/*==================[inclusions]=============================================*/
#include "prueba_final_maestro.h"       /* <= own header */
#include "gpio.h"
#include "systemclock.h"
#include "delay.h"
#include "switch.h"
#include "led.h"
#include "timer.h"
#include "MMA7260Q.h"
#include "HC05.h"
/*==================[macros and definitions]=================================*/

#define ROT_0	'0'
#define ROT_90	'1'

#define INTERVALO 500
#define ON 1
#define OFF 0
#define DIEZ_POR_CIENTO 10/100

float actual_eje_x;
float actual_eje_y;
float actual_eje_z;

//float anterior_eje_x=0;
//float anterior_eje_y=0;
//float anterior_eje_z=0;

uint8_t rotacion=ROT_0;
uint8_t bandera_medicion=OFF;

int16_t promediacion;
int16_t umbral_movimiento_cabeza;

int16_t suma_vectores_actual;
int16_t suma_vectores_anterior=0;

/*==================[internal data definition]===============================*/
void Medir();
timer_config my_timer = {TIMER_A,INTERVALO,&Medir};

/*==================[internal functions declaration]=========================*/


int16_t GetVectorAceleracion(){
	int16_t sumatoria_vectores_aceleracion=actual_eje_x+actual_eje_y+actual_eje_z;


	return sumatoria_vectores_aceleracion;


}



void Medir(){
	bandera_medicion=!bandera_medicion;
	LedOn(LED_1);
	actual_eje_x=ReadXValue();
	actual_eje_y=ReadYValue();
	actual_eje_z=ReadZValue();


}


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/
/*
 *
 */

void SisInit(){
	hc05_config puerto;
	puerto.baud_rate=115200;
	puerto.rol=RolMaestro;
	puerto.p_serial=NULL;


	//inits
	SystemClockInit();
	SwitchesInit();
	LedsInit();

	HC05Init(&puerto);
	MMA7260QInit(GPIO_T_FIL0,GPIO_T_FIL2);

	TimerInit(&my_timer);
	TimerStart(TIMER_A);

}

int main(void){

	SisInit();

	while(1){

		if(bandera_medicion){
			LedOff(LED_1);
			suma_vectores_actual=GetVectorAceleracion();
			umbral_movimiento_cabeza=suma_vectores_actual*DIEZ_POR_CIENTO;

		if(rotacion == ROT_0){
			if ((suma_vectores_actual-suma_vectores_anterior) >= umbral_movimiento_cabeza){

				suma_vectores_anterior=suma_vectores_actual;


					HC05SendByte(ROT_90);
				}
				rotacion=ROT_90;


			}

			else{

				rotacion =ROT_0;
				HC05SendByte(ROT_0);

			}
			bandera_medicion=OFF;

		}// cierre if bandera

	}


	return 0;
}

/*==================[end of file]============================================*/

