﻿Descripción del Proyecto:


Mi aplicacion para personas paraplejicas, permite la apertura de puertas con leves movimientos de la cabeza.

Para esto empleo 2 aplicaciones distintas, una aplicacion Maestra, la cual es la que describire a continuacion y una aplicacion
Esclava que sera descripta en su apartado correspondiente.

Para el caso de la aplicacion maestra, esta detecta los movimientos de cabeza mediante las variaciones de aceleracion detectadas
por el acelerometro digital MMA7260Q. Posteriormete, procesa estos valores y envia una orden mediante el sensor Bluetooth a la 
aplicacion Esclava, con esta orden se podra abrir y/o cerrar puertas de manera eficiente y para la persona con discapacidad sera una 
facilidad en su vida cotidiana.


Traversaro Julian.
