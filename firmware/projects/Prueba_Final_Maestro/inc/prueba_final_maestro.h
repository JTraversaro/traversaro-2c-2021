/*! @mainpage Aplicacion Final Maestra
 *
 * \section genDesc General Description
 *
 *  Aplicacion final Maestra, la cual debe ser colocada en el casco de la persona
 * con la cual controlara la apertura y cierre de la puerta
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
  * |    MMA7260Q 	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * |   	  PINX	 	| 	   CH1		|
 * |   	  PINY	 	| 	   CH2 		|
 * |   	  PINZ	 	| 	   CH3		|
 * |   	  GS1	 	|    T_FIL0 	|
 * |   	  GS2	 	| 	 T_FIL2		|
 * |   	  VCC	 	| 	  +3.3V		|
 * |   	  GND	 	| 	   GND		|
 * |   	  SM	 	| 	  +3.3V		|
 *
 ** |    HC05	 	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * |   	  PINRX	 	| 	   232_TX	|
 * |   	  PINTX	 	| 	   232_RX	|
 * |   	  VCC	 	| 	  +3.3V		|
 * |   	  GND	 	| 	   GND		|
 *
 * @author Traversaro Julian
 *
 * 		juliantraversaro@gmail.com
 *
 *
 */
#ifndef _PRUEBA_FINAL_MAESTRO_H
#define _PRUEBA_FINAL_MAESTRO_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _PRUEBA_FINAL_MAESTRO_H */

