/*
 *
 * Prueba de acelerometro	HC05
 *
 * @author Traversaro Julian
 *
 * |    HC05		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * |   	  PINRX	 	| 	   232_TX	|
 * |   	  PINTX	 	| 	   232_RX	|
 * |   	  VCC	 	| 	  +3.3V		|
 * |   	  GND	 	| 	   GND		|
 *
 */

/*==================[inclusions]=============================================*/
#include "prueba_0_bluetooth_esclavo.h"       /* <= own header */

#include "gpio.h"
#include "systemclock.h"
#include "delay.h"
#include "switch.h"
#include "timer.h"
#include "led.h"
#include "MMA7260Q.h"
#include "uart.h"
#include "HC05.h"
#include "SG_90.h"
/*==================[macros and definitions]=================================*/
float eje_x;
float eje_y;
float eje_z;
uint8_t byte_maestro;
uint8_t Rotacion;
#define MEDICION 500
/*==================[internal data definition]===============================*/
void Rotar();
void Maestro();

hc05_config puerto={115200,RolEsclavo,&Maestro};

serial_config UART_PC={SERIAL_PORT_PC, 115200, NULL};


/*==================[internal functions declaration]=========================*/
// setea la altura de una persona

void Rotar(){

			if (Rotacion ==0){
				LedOn(LED_RGB_B);
			}
			if(Rotacion==1){
				LedOn(LED_RGB_R);
			}
}

void Maestro(){
	HC05ReadByte(&byte_maestro);
	UartSendString(SERIAL_PORT_PC, "Interfaz de prueba de Recepcion de bit Bluetooth\r\n");
	UartSendString(SERIAL_PORT_PC, UartItoa((byte_maestro),10));

	switch(byte_maestro){
	case 2:
		Rotacion=0;
		Rotar();
		break;
	case 1:
		Rotacion=1;
		Rotar();
		break;
	}
}
/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/
/*
 *
 */
int main(void){


	//inits
	SystemClockInit();
	SwitchesInit();
	LedsInit();
	HC05Init(&puerto);
	UartInit(&UART_PC);


	while(1){

	}

	return 0;
}

/*==================[end of file]============================================*/

