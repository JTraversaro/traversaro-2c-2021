/*
 *
 * Prueba de servomotor SG_90
 *
 * @author Traversaro Julian
 *
 */

/*==================[inclusions]=============================================*/
#include "prueba_0_servomotor.h"       /* <= own header */

#include "gpio.h"
#include "systemclock.h"
#include "delay.h"
#include "switch.h"
#include "SG_90.h"
/*==================[macros and definitions]=================================*/
#define TIEMPOMS 100
#define ROTAR_0
#define ROTAR_90
#define ROTAR_180

uint8_t teclas;

/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){
	//inits
	SystemClockInit();
	SG90Init();
	SwitchesInit();
	LedsInit();


	// inicia en 0°
	SG90On();

	while(1){
		teclas=SwitchesRead();
		switch(teclas){
		case SWITCH_1:
			SG90Rotar(ROTACION_90);
			break;
		case SWITCH_2:
			SG90Rotar(ROTACION_180);
			break;
		case SWITCH_3:
			SG90Rotar(ROTACION_0);
		break;
		}

	}

	return 0;
}

/*==================[end of file]============================================*/

