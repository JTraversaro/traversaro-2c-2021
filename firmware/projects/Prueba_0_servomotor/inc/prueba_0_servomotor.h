/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup	Aplicacion de prueba
 ** @{ */
/** \addtogroup SG_90
 ** @{ */

/**
 **	** Falta agregar a SG_90.h una configuracion para interrupciones **
 **
 ** 	Aplicacion de prueba para servomotor SG_90
 **
 ** Se configuran 3 distintas rotaciones a 0°, 90° y 180°, siendo la primera y la ultima de estas sus extremos de funcionamiento
 **
 **

 * Initials     Name
 * ---------------------------
 *	TJ			Traversaro Julian

 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	Signal_pin 	| 	TFIL_2		|
 * | 	5V		 	| 	5 V			|
 * | 	GND		 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 25/10/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Traversaro Julian
 *
*/
#ifndef _PRUEBA_0_SERVOMOTOR_H
#define _PRUEBA_0_SERVOMOTOR_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _PRUEBA_0_SERVOMOTOR_H */

