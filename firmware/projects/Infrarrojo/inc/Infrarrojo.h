/*! @mainpage Infrarrojo
 *
 * \section genDesc General Description
 *
 *
 *	Proyecto en el el que se emplea el sensor infrarrojo Tcrt5000
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO0		|
 * | 	PIN2	 	| 	5 V		|
 * | 	PIN3	 	| 	GND		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Traversaro Julian
 *
*/
#ifndef _INFRARROJO_H
#define _INFRARROJO_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _INFRARROJO_H */

