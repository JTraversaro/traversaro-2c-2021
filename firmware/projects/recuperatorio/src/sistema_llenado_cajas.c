/*! @mainpage sistema de llenado de cajas
 *	Traversaro Julian
 *	APLICACION PARA EXEMEN FINAL DE CUATRIMESTRE 2C- 2021
 * @section changelog Changelog
 *
 *
 * | 	Tcrt5000	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO0		|
 * |	Vcc			|	5 V	     	|
 * |	GND			|	GND			|
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 1/11/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Traversaro Julian
 */

/*==================[inclusions]=============================================*/
#include "sistema_llenado_cajas.h"       /* <= own header */



/*==================[macros and definitions]=================================*/

#define ON 1
#define OFF 0
#define INTERVALO 200


#define MAX_VOLTAGE 3300 //3,3 V
#define MIN_VOLTAGE 0

#define MAX_VALUE 1024

#define SIGUIENTE 1
#define ANTERIOR 0
#define MILISEGUNDOS 1000
#define MAXIMO_LOTE	15

bool TEC1=OFF;
bool TEC2=ON;
bool TEC3=OFF;


int16_t maximo=0;
int16_t minimo=0;
int16_t tiempo_de_llenado=0;//en ms
int16_t numero_caja=0;
bool infrarrojo=OFF;


//mV
uint16_t pesoMaximo=3300;
uint16_t pesoMinimo=0;
uint16_t maximo_por_caja=440;//20 kg por regla de 3, si 150 kg son 3300 mV 20 son 440 mV

float peso_en_volt=0;

/*==================[internal data definition]===============================*/



serial_config lectura ={SERIAL_PORT_PC,115200,NULL};

/*==================[internal functions declaration]=========================*/

bool Tec1(){
	//PRENDO-APAGO
	TEC1=!TEC1;
	return true;
}


int16_t Maximo(int16_t tiempo_de_llenado){
	int16_t maximo_tiempo=0;

	if(maximo_tiempo<tiempo_de_llenado){
		maximo_tiempo=tiempo_de_llenado;
	}


	return maximo_tiempo;
}

int16_t Minimo(int16_t tiempo_de_llenado){
	int16_t minima_tiempo=0;
	if(minima_tiempo>tiempo_de_llenado){
		minima_tiempo=tiempo_de_llenado;
	}


	return minima_tiempo;
}


void SisInit(void)
{
	//Inits
	SystemClockInit();
	LedsInit();
	SwitchesInit();

	Tcrt5000Init(GPIO_T_COL0);
	BALANZAInit(pesoMaximo,pesoMinimo);


	UartInit(&lectura);

	SwitchActivInt(SWITCH_1, Tec1);
	SwitchActivInt(SWITCH_2, Tec1);

}


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){

	SisInit();

	while(1){



		if (TEC1==ON){
			LedOn(LED_1);//enciendo cinta 1
			infrarrojo=Tcrt5000State();
			if(infrarrojo && numero_caja <MAXIMO_LOTE){

				numero_caja++;
				LedOff(LED_1);//apago cinta 1
				LedOn(LED_2);//prendo cinta 2
				peso_en_volt= BALANZAReadValue();

				if (peso_en_volt<maximo_por_caja){
					LedOn(LED_2);//si no pesa 20 sigue prendida
					tiempo_de_llenado++;
				}
				else{
					LedOff(LED_2);//apago cinta 2
					peso_en_volt=0;
					//calculo los tiempos de esta caja
					maximo=Maximo(tiempo_de_llenado);
					minimo=Minimo(tiempo_de_llenado);

					//informo por uart numero de caja y tiempo de llenado en segundos
					tiempo_de_llenado=tiempo_de_llenado/MILISEGUNDOS;
					UartSendString(SERIAL_PORT_PC, " Tiempo de llenado de caja \n\r");
					UartSendString(&lectura, UartItoa(numero_caja, 10));
					UartSendString(&lectura, UartItoa(tiempo_de_llenado, 10));
					UartSendString(SERIAL_PORT_PC, " seg \n\r");

					LedOn(LED_1);// prendo cinta 1
				}
				infrarrojo=OFF;
			}//cierro infrarrojo
			else{
				//cuando se sobrepasan las 15 cajas, informo tiempos maximos y minimos
				numero_caja=0;
				maximo=maximo/MILISEGUNDOS;
				UartSendString(SERIAL_PORT_PC, " Tiempo de llenado maximo \n\r");
				UartSendString(&lectura, UartItoa(maximo, 10));
				UartSendString(SERIAL_PORT_PC, " seg \n\r");
				minimo=minimo/MILISEGUNDOS;
				UartSendString(SERIAL_PORT_PC, " Tiempo de llenado minimo \n\r");
				UartSendString(&lectura, UartItoa(minimo, 10));
				UartSendString(SERIAL_PORT_PC, " seg \n\r");



			}

		}//cierro cinta

	}
	return 0;

}




/*==================[end of file]============================================*/

