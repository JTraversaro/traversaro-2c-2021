/*! @mainpage sistema de llenado de cajas
 *	Traversaro Julian
 *	APLICACION PARA EXEMEN FINAL DE CUATRIMESTRE 2C- 2021
 * @section changelog Changelog
 *
 *
 * | 	Tcrt5000	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO0		|
 * |	Vcc			|	5 V	     	|
 * |	GND			|	GND			|
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 1/11/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Traversaro Julian
 */


#ifndef _SISTEMA_LLENADO_CAJAS_H
#define _SISTEMA_LLENADO_CAJAS_H
#include "gpio.h"
#include "systemclock.h"
#include "bool.h"
#include "led.h"
#include "Tcrt5000.h"
#include "switch.h"
#include "timer.h"
#include "uart.h"
#include "balanza.h"

/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _SISTEMA_LLENADO_CAJAS_H */

