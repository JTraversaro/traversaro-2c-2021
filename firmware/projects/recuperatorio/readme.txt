﻿/*! @mainpage sistema de llenado de cajas
 *	Traversaro Julian
 *	APLICACION PARA EXEMEN FINAL DE CUATRIMESTRE 2C- 2021
 * @section changelog Changelog
 *
 *
 * | 	Tcrt5000	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO0		|
 * |	Vcc			|	5 V	     	|
 * |	GND			|	GND			|
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 1/11/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Traversaro Julian
 */
