/*! @mainpage Sonar
 *	Traversaro Julian
 *	APLICACION PARA EXEMEN FINAL DE CUATRIMESTRE 2C- 2021
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 1/11/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Traversaro Julian
 */

/*==================[inclusions]=============================================*/
#include "sonar.h"       /* <= own header */
#include "gpio.h"
#include "systemclock.h"
#include "bool.h"
#include "led.h"
#include "goniometro.h"
#include "hc_sr4.h"
#include "switch.h"
#include "timer.h"
#include "uart.h"

/*==================[macros and definitions]=================================*/

#define ON 1
#define OFF 0
#define INTERVALO 200


#define MAX_VOLTAGE 3300 //3,3 V
#define MIN_VOLTAGE 0

#define MAX_VALUE 1024

#define SIGUIENTE 1
#define ANTERIOR 0


bool TEC1=OFF;
bool TEC2=ON;
bool TEC3=OFF;


int16_t minima_dist=0;
int16_t grados_medidos;
int16_t distancia=0;
int16_t cantidad_obj=0;
uint8_t dato_teclado;

typedef enum{
	ANGULO_0=0,
	ANGULO_15,
	ANGULO_30,
	ANGULO_45,
	ANGULO_60,
	ANGULO_75
}angulos_t;

/*==================[internal data definition]===============================*/
void StartBarrido();

void InformoMinimo();

timer_config my_timer = {TIMER_A, INTERVALO , &StartBarrido};
//timer_config my_timer2= {TIMER_B,6*INTERVALO,&InformoMinimo};
serial_config lectura ={SERIAL_PORT_PC,115200,NULL};
angulos_t posicion=ANGULO_0;
/*==================[internal functions declaration]=========================*/


void MinimoAnguloMedido(int16_t minimo){
	int16_t minima_distancia=0;
	int i;
	for(i=0;i<ANGULO_75;i++){

		if(minima_distancia>minimo){
			minima_distancia=minimo;
		}


	}
	minima_dist=minima_distancia;
}



void Servomotor(uint8_t posicion_actual){
	int i;
	if(posicion_actual== SIGUIENTE ){
		//giro
		LedOn(LED_RGB_R);
		posicion++;
	}
	else{
		if(posicion_actual==ANTERIOR){
			//giro
			LedOn(LED_RGB_G);
			posicion--;
			//borro objetos mas cercano
			for(i=0;i<ANGULO_75;i++){
				minima_dist=0;
			}
		}


	}
}

void StartBarrido(){
	grados_medidos=GONIOMETROReadValue();

	switch (posicion){
	case ANGULO_0:
		distancia = HcSr04ReadDistanceCentimeters();		/*PRIMERO MEDIR ANGULO, DESPUES DISTANCIA */

		UartSendString(&lectura, UartItoa(grados_medidos, 10));
		UartSendString(SERIAL_PORT_PC, " ° objeto a\n\r");
		UartSendString(&lectura, UartItoa(distancia, 10));
		UartSendString(SERIAL_PORT_PC, " cm \n\r");
		MinimoAnguloMedido(distancia);
		InformoMinimo();

		Servomotor(SIGUIENTE);

		break;

	case ANGULO_15:
		distancia = HcSr04ReadDistanceCentimeters();

		UartSendString(&lectura, UartItoa(grados_medidos, 10));
		UartSendString(SERIAL_PORT_PC, " ° objeto a \n\r");
		UartSendString(&lectura, UartItoa(distancia, 10));
		UartSendString(SERIAL_PORT_PC, " cm \n\r");
		MinimoAnguloMedido(distancia);
		Servomotor(SIGUIENTE);

		break;
	case ANGULO_30:
		distancia = HcSr04ReadDistanceCentimeters();

		UartSendString(&lectura, UartItoa(grados_medidos, 10));
		UartSendString(SERIAL_PORT_PC, " °  \n\r");
		UartSendString(&lectura, UartItoa(distancia, 10));
		UartSendString(SERIAL_PORT_PC, " cm \n\r");
		MinimoAnguloMedido(distancia);
		Servomotor(SIGUIENTE);
		break;

	case ANGULO_45:
		distancia = HcSr04ReadDistanceCentimeters();

		UartSendString(&lectura, UartItoa(grados_medidos, 10));
		UartSendString(SERIAL_PORT_PC, " ° \n\r");
		UartSendString(&lectura, UartItoa(distancia, 10));
		UartSendString(SERIAL_PORT_PC, " cm \n\r");
		MinimoAnguloMedido(distancia);
		Servomotor(SIGUIENTE);
		break;

	case ANGULO_60:
		distancia = HcSr04ReadDistanceCentimeters();

		UartSendString(&lectura, UartItoa(grados_medidos, 10));
		UartSendString(SERIAL_PORT_PC, " °  \n\r");
		UartSendString(&lectura, UartItoa(distancia, 10));
		UartSendString(SERIAL_PORT_PC, " cm \n\r");
		MinimoAnguloMedido(distancia);
		Servomotor(SIGUIENTE);
		break;

	case ANGULO_75:
		distancia = HcSr04ReadDistanceCentimeters();
		UartSendString(&lectura, UartItoa(grados_medidos, 10));
		UartSendString(SERIAL_PORT_PC, " ° \n\r");
		UartSendString(&lectura, UartItoa(distancia, 10));
		UartSendString(SERIAL_PORT_PC, " cm \n\r");
		MinimoAnguloMedido(distancia);
		InformoMinimo();
		Servomotor(ANTERIOR);
		break;
	}


}


void InformoMinimo(){


	UartSendString(&lectura, UartItoa(posicion, 10));
	UartSendString(SERIAL_PORT_PC, " ° MIN\n\r");
	UartSendString(&lectura, UartItoa(minima_dist, 10));
	UartSendString(SERIAL_PORT_PC, " cm \n\r");

}



void SisInit(void)
{
	//Inits
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	GONIOMETROInit();
	HcSr04Init(GPIO_T_FIL2,GPIO_T_FIL3);


	TimerInit(&my_timer);
	//TimerInit(&my_timer2);
	TimerStart(TIMER_A);
	//TimerStart(TIMER_B);
	UartInit(&lectura);

}


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){

	SisInit();

	while(1){

	}
	return 0;

}




/*==================[end of file]============================================*/

