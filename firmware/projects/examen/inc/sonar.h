/*! @mainpage Sonar
 *	Traversaro Julian
 *	APLICACION PARA EXEMEN FINAL DE CUATRIMESTRE 2C- 2021
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 1/11/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Traversaro Julian
 */


#ifndef _SONAR_H
#define _SONAR_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _SONAR_H */

