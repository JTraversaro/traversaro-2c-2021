﻿Descripción del Proyecto.
CONEXIONADO DE PERIFERICOS

	|   Ultrasound	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	ECHO	 	| 	 T_FIL2	    |
 * | 	TRIGGER	 	| 	 T_FIL3		|
 * | 	+5V 	 	| 	  +5V    	|
 * | 	GND	    	| 	  GND		|
 
 
   |  POTENCIOMETRO |   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | PIN DERECHO	| 	 +3.3V	    |
 * | PIN IZQUIERDO 	| 	 GND		|
 * | PIN MEDIO 	 	| 	  CH1    	|
 
 
 TAMBIEN SE UTILIZA UN GONIOMETRO Y UN SERVOMOTOR
 