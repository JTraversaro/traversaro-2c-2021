/*
 *
 * Prueba de acelerometro	MMA7260Q
 *
 * @author Traversaro Julian
 *
 * |    MMA7260Q 	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * |   	  PINX	 	| 	   CH1		|
 * |   	  PINY	 	| 	   CH2 		|
 * |   	  PINZ	 	| 	   CH3		|
 * |   	  GS1	 	|    T_FIL0 	|
 * |   	  GS2	 	| 	 T_FIL2		|
 * |   	  VCC	 	| 	  +3.3V		|
 * |   	  GND	 	| 	   GND		|
 * |   	  SM	 	| 	  +3.3V		|
 *
 */

/*==================[inclusions]=============================================*/
#include "prueba_0_acelerometro.h"       /* <= own header */

#include "gpio.h"
#include "systemclock.h"
#include "delay.h"
#include "switch.h"
#include "led.h"
#include "MMA7260Q.h"
/*==================[macros and definitions]=================================*/
float eje_x;
float eje_y;
float eje_z;

float diferencial_x;
float diferencial_y;
float diferencial_z;

#define DELAY 100
/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/
// setea la altura de una persona
void setEjes(){

	eje_x=ReadXValue();
	eje_y=ReadYValue();
	eje_z=ReadZValue();
}
void setDiferenciales(float x, float y, float z){
	diferencial_x=0.1*x;
	diferencial_y=0.1*y;
	diferencial_z=0.1*z;
}

float getX(){
	return eje_x;
}
float getY(){
	return eje_y;
}

float getZ(){
	return eje_z;
}

/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/
/*
	 * Posibles problemas de esto:
	 *  aceleraciones negativas ----> calcularle el modulo ???????
	 * mal implementacion de algo ---> pedir ayuda
	 * preguntar por coneccionado, no encontre T_COL1
	 *
	 * */
int main(void){
	//inits
	SystemClockInit();
	SwitchesInit();
	LedsInit();


	MMA7260QInit(GPIO_T_FIL0,GPIO_T_FIL2);	//preguntar por COL0 Y COL1


	/*seteo ejes*/
	  setEjes();
	 float x_actual=getX();
	 float y_actual=getY();
	 float z_actual=getZ();
	  /*variables auxiliares	10% del total seteado inicialmente */
	 setDiferenciales(x_actual,y_actual,z_actual);

	while(1){
		setEjes();

		// si la diferencia entre el valor actual y el valor anterior medido es mayor a 10% en los 3 ejes ---> realiza una accion


		if(eje_x-x_actual>=diferencial_x && eje_y-y_actual>=diferencial_y && eje_z-z_actual>=diferencial_z){
			LedOn(LED_RGB_G);
			x_actual=eje_x;
			y_actual=eje_y;
			z_actual=eje_z;
			setDiferenciales(eje_x,eje_y,eje_z);
		}

		else{
			LedOn(LED_RGB_R);

		}

		DelayMs(DELAY);
	}

	return 0;
}

/*==================[end of file]============================================*/

