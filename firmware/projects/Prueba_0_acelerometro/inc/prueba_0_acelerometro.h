/**	Traversaro Julian
 *
 *  \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup	Aplicacion de prueba
 ** @{ */
/** \addtogroup MMA7260Q
 ** @{ */

/**
 **	**
 **
 ** 	Aplicacion de prueba para sensor de aceleracion MMA7260Q
 **
 ** Configuro una pueba inicial en la que mido la aceleracion inciaial y luego mido cada 100 ms, si la aceleracion en los 3 ejes cambia mas de un 10%
 ** prendo un led verde
 ** sino prendo uno rojo
 **
 **

 * Initials     Name
 * ---------------------------
 *	TJ			Traversaro Julian

 *
 * |    MMA7260Q 	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * |   	  PINX	 	| 	   CH1		|
 * |   	  PINY	 	| 	   CH2 		|
 * |   	  PINZ	 	| 	   CH3		|
 * |   	  GS1	 	|    T_COL0 	|
 * |   	  GS2	 	| 	 T_COL1		|
 * |   	  VCC	 	| 	  +3.3V		|
 * |   	  GND	 	| 	   GND		|
 * |   	  SM	 	| 	  +3.3V		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 26/10/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Traversaro Julian
 *
*/
#ifndef _PRUEBA_0_ACELEROMETRO_H
#define _PRUEBA_0_ACELEROMETRO_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _PRUEBA_0_ACELEROMETRO_H */

