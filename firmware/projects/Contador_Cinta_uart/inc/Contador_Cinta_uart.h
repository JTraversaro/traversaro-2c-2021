/*! @mainpage Contador_Cinta
 *
 * \Contador de objetos en cinta transportadora

 *
 * Esta aplicacion cuenta objetos, con un rango maximo de 16 objetos, y enciende en codigo binario los led de la placa
 *
 * \section hardConn Hardware Connection
 *
 * | 	Tcrt5000	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO0		|
 * |		Vcc		|		5 V		|
 * |	GND			|	GND			|
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 13/9/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Traversaro Julian
*/

#ifndef _CONTADOR_CINTA_UART_H
#define _CONTADOR_CINTA_UART_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _CONTADOR_CINTA_UART_H */

