/*! @mainpage Blinking
 *
 * \section genDesc General Description
 *
 * Aplicacion de uso del display lcd
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 				|
 * | 	PIN2	 	| 				|
 * | 	PIN3	 	| 				|
 * |  	PIN4	 	| 				|
 * |  	PIN5	 	| 				|
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 19/10/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Traversaro Julian
 *
 */

#ifndef DISPLAY_H
#define DISPLAY_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef DISPLAY_H */

