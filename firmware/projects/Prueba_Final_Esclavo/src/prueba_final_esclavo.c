/*
 *
 * Aplicacion final para la placa Esclaba, la cual recibe la orden por Bluetooth de abrir o cerrar una puerta
 * lo cual es simunlado mediante un servomotor
 *
 * @author Traversaro Julian
 *
*  |    SG 90		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	Signal_pin 	| 	TFIL_2		|
 * | 	5V		 	| 	5 V			|
 * | 	GND		 	| 	GND			|
 *
 *
 * |    HC05		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * |   	  PINRX	 	| 	   232_TX	|
 * |   	  PINTX	 	| 	   232_RX	|
 * |   	  VCC	 	| 	  +3.3V		|
 * |   	  GND	 	| 	   GND		|
 *
 */

/*==================[inclusions]=============================================*/
#include "prueba_final_esclavo.h"       /* <= own header */
#include "systemclock.h"
#include "switch.h"
#include "timer.h"
#include "led.h"
#include "uart.h"

#include "MMA7260Q.h"
#include "HC05.h"
#include "SG_90.h"

/*==================[macros and definitions]=================================*/


#define MEDICION 500
#define ON 1
#define OFF 0

uint8_t orden =OFF;
uint8_t byte_maestro;
uint8_t Rotacion;
uint8_t prueba_lectura;
uint8_t ByteMaestro_ASCI;
/*==================[internal data definition]===============================*/
void Rotar();
void Maestro();

hc05_config puerto={115200,RolEsclavo,&Maestro};

serial_config UART_PC={SERIAL_PORT_PC, 115200, NULL};


/*==================[internal functions declaration]=========================*/


void Rotar(){

	if (Rotacion ==OFF){
		LedOff(LED_RGB_R);
		LedOn(LED_RGB_B);
		SG90Rotar(ROTACION_0);
	}
	if(Rotacion==ON){
		LedOff(LED_RGB_B);
		LedOn(LED_RGB_R);
		SG90Rotar(ROTACION_90);

	}
}

void Maestro(){
	orden=!orden;

	prueba_lectura = HC05ReadByte(&byte_maestro);

}
/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/
/*
 *
 */

void SisInit(){
	//inits
	SystemClockInit();
	SwitchesInit();
	LedsInit();
	HC05Init(&puerto);
	UartInit(&UART_PC);
	SG90Init();
	SG90On();

}
int main(void){

	SisInit();

	while(1){
		if(orden){

			if (prueba_lectura){
				LedOn(LED_2);
				//ByteMaestro_ASCI= UartItoa((byte_maestro),10);

			}
			else{
				LedOn(LED_3);
			}

			UartSendString(SERIAL_PORT_PC, "Interfaz de prueba de Recepcion de bit Bluetooth\r\n");
			UartSendString(SERIAL_PORT_PC, UartItoa((byte_maestro),10));

			switch(byte_maestro){
			case '0':
				Rotacion=OFF;
				Rotar();
				break;
			case '1':
				Rotacion=ON;
				Rotar();
				break;
			}
			/* PRUEBA DE RECEPCION DE BYTE
			switch(ByteMaestro_ASCI){
					case '0':
						Rotacion=OFF;
						Rotar();
						break;
					case '1':
						Rotacion=ON;
						Rotar();
						break;
					}*/
			orden=OFF;
		}


	}

	return 0;
}

/*==================[end of file]============================================*/

