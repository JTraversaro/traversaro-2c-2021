/*! @mainpage Aplicacion Final Maestra
 *
 * \section genDesc General Description
 *
 *  Aplicacion final para la placa Esclaba, la cual recibe la orden por Bluetooth de abrir o cerrar una puerta
 * lo cual es simunlado mediante un servomotor
 *
 * \section hardConn Hardware Connection
 * |    SG 90		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	Signal_pin 	| 	TFIL_2		|
 * | 	5V		 	| 	5 V			|
 * | 	GND		 	| 	GND			|
 *
 * |    HC05	 	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * |   	  PINRX	 	| 	   232_TX	|
 * |   	  PINTX	 	| 	   232_RX	|
 * |   	  VCC	 	| 	  +3.3V		|
 * |   	  GND	 	| 	   GND		|
 *
 *
 * @section changelog Changelog
 *
  *
 *
 * @author Traversaro Julian
 *
 * 		juliantraversaro@gmail.com
 *
 *
 */
#ifndef _PRUEBA_FINAL_ESCLAVO_H
#define _PRUEBA_FINAL_ESCLAVO_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _PRUEBA_FINAL_ESCLAVO_H */

