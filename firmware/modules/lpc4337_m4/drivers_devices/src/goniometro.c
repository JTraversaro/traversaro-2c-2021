/* Copyright 2021,
 *
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** @brief driver
 **
 **/

/*
 * Initials     Name
 * ---------------------------
 *  TJ			Traversaro Julian
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20210609 v0.0.1 initials initial version
 */

/*==================[inclusions]=============================================*/
#include "goniometro.h"

/*==================[macros and definitions]=================================*/
/** @def MAX_VOLTAGE
 * @brief Máximo valor de voltaje en mV que puede medir el driver
 */
#define MAX_VOLTAGE 3300
/** @def MAX_VALUE
 * @brief Máximo valor de bits
 */
#define MAX_VALUE 1024

#define MAX_GRADOS 75

#define GRADOS MAX_GRADOS/MAX_VOLTAGE

#define MAX_UMBRAL_VOLTAGE	2100	//mV
#define MIN_UMBRAL_VOLTAGE	1200	//mV

/*==================[internal data declaration]==============================*/

bool conversion; /**< booleando si realiza o no la conversión*/
uint16_t valor; /**< Valor convertido*/
uint8_t canal; /**< Canal de la placa*/





/*==================[internal functions declaration]=========================*/

/**@fn void ReadValue()
 * @brief  Función que lee el valor analógico de determinado canal
 * @return None
 */
void ReadValue();
/**@fn float UnitConvert(uint16_t value)
 * @brief  Función que convierte el valor leído grados
 * @return valor convertido
 */
float UnitConvert(uint16_t value);

/*==================[internal data definition]===============================*/



/*==================[external data definition]===============================*/

analog_input_config canal_1 = {CH1,AINPUTS_SINGLE_READ,ReadValue}; /**< Typedef configuración puerto analógico/digital canal x*/


/*==================[internal functions definition]==========================*/

void ReadValue(){
	conversion=false;
	AnalogInputRead(canal,&valor);
}

float UnitConvert(uint16_t value){

	float volts;
	float grados;

	volts =value*MAX_VOLTAGE/MAX_VALUE;
	grados =75*(volts-MIN_UMBRAL_VOLTAGE)/(MAX_UMBRAL_VOLTAGE-MIN_UMBRAL_VOLTAGE);

	return grados;
}

/*==================[external functions definition]==========================*/

bool GONIOMETROInit(){
	AnalogInputInit(&canal_1);
	return 1;
}

float GONIOMETROReadValue(){
	conversion = true;
	canal = CH1;
	AnalogStartConvertion();
	while(conversion){	//chequea finalizacion de convercion
		}


	return UnitConvert(valor);
}


bool GONIOMETRODeinit(){

	GPIODeinit();
	return 1;
}
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/
