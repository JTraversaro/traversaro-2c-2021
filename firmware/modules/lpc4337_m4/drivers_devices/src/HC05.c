/* Copyright 2016,
 * Leandro D. Medus
 * lmedus@bioingenieria.edu.ar
 * Eduardo Filomena
 * efilomena@bioingenieria.edu.ar
 * Juan Manuel Reta
 * jmrera@bioingenieria.edu.ar
 * Sebastian Mateos
 * smateos@ingenieria.uner.edu.ar
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** \
 **
 **/

/*
 * Initials     Name
 * ---------------------------
 *	JT			Traversaro Julian
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 *
 */

/*==================[inclusions]=============================================*/
#include "delay.h"
#include "gpio.h"
#include "HC05.h"
#include "uart.h"
/*==================[macros and definitions]=================================*/




/*==================[internal data declaration]==============================*/

serial_config global;


/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/


/** \brief Function to turn on a specific led */
void HC05Init(hc05_config *puerto){
											// hago un hc05_t porque quiero que solo pase los baudios, SERIAL_PORT_P2_CONNECTOR va siempre y no otro
	global.baud_rate =puerto->baud_rate;
	global.port=SERIAL_PORT_P2_CONNECTOR;
	global.pSerial=puerto->p_serial;

	UartInit(&global);

	}

uint32_t HC05ReadStatus(){
	uint32_t estado;
	estado=UartReadStatus(global.port);

	return estado;
}


uint32_t HC05RxReady(){
	uint32_t rx_ready;
	rx_ready=UartRxReady(global.port);

	return rx_ready;
}

uint8_t HC05ReadByte(uint8_t *dat){
	uint8_t read_byte;
	read_byte=UartReadByte(global.port,dat);

	return read_byte;
}

void HC05SendByte(uint8_t* dat){
	UartSendByte(global.port,dat);
}

void HC05SendString(uint8_t *msg){
	UartSendString(global.port,&msg);
}


void HC05SendBuffer( const void *data, uint8_t nbytes){

	UartSendBuffer(global.port,data,nbytes);

}

uint8_t* HC05Itoa(uint32_t val, uint8_t base){

	return UartItoa(val,base);

}
/*==================[end of file]============================================*/
