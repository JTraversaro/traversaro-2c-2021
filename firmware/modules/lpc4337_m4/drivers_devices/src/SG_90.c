/* Copyright 2016,
 *
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** \brief Bare Metal driver the EDU-CIAA board.
 **
 **/

/*
 * Initials     Name
 * ---------------------------
 *	TJ			Traversaro Julian

*
 * modification history (new versions first)
 * -----------------------------------------------------------

 */

/*==================[inclusions]=============================================*/
#include "delay.h"
#include "gpio.h"
#include "SG_90.h"
#include "pwm_sct.h"
/*==================[macros and definitions]=================================*/


/*	porcentaje del ciclo de trabajo total
 * 	1 ms es el 5%
 * 	2 ms es el 10%
 * 	*/
#define ROT_0	2
#define ROT_90	8
#define ROT_180 12




//pin TFIL_2
pwm_out_t pines_outputs={CTOUT0};

/*==================[internal data declaration]==============================*/
#define C_OUTPUS 1
#define PERIODO 0.02	//20 ms
#define FRECUENCIA	1/PERIODO	//50hz


/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/


/** \brief Function to turn on a specific led */

void SG90Init(){

	PWMInit(&pines_outputs,C_OUTPUS,FRECUENCIA);


}

void SG90On(){
	// inicio y seteo a cero grados
	PWMSetDutyCycle(pines_outputs,ROT_0);
	PWMOn();

}

void SG90Rotar(rotaciones_t rotacion){
	//Envio un pulso

	// si envia un pulso no valido, queda en 0°

	if (rotacion != ROTACION_0 && rotacion !=ROTACION_90 && rotacion !=ROTACION_180){
		rotacion=ROTACION_0;
	}

	switch(rotacion){
	case ROTACION_0:
		PWMSetDutyCycle(pines_outputs,ROT_0);
		break;

	case ROTACION_90:
		PWMSetDutyCycle(pines_outputs,ROT_90);
		break;
	case ROTACION_180:
		PWMSetDutyCycle(pines_outputs,ROT_180);
		break;
	}

}

void SG90Deinit(){
	//GPIODeinit(void);
}
/*==================[end of file]============================================*/
