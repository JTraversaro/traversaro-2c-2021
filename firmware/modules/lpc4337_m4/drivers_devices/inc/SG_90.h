/*	Traversaro Julian
 *	juliantraversaro@gmail.com
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef SG_90_H
#define SG_90_H


/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Devices Drivers devices
 ** @{ */
/** \addtogroup SG_90
 ** @{ */

/** @brief Bare Metal header for EDU-CIAA NXP
 **
 ** Driver para servomotor SG_90
 ** se configuran 3 distintas rotaciones a 0°, 90° y 180°, siendo la primera y la ultima de estas sus extremos de funcionamiento
 **
 **	|	Parametro no configurable	|
 **	|:	-----------:|:-------------:|
 **	|	Signal_pin	|	TFIL_2		|
 **/
/*
 * Initials     Name
 * ---------------------------
 *	TJ			Traversaro Julian
*

 *
 * modification history (new versions first)
 * -----------------------------------------------------------

 */


/*==================[inclusions]=============================================*/
#include "bool.h"
#include <stdint.h>
#include "delay.h"

/*==================[macros]=================================================*/

/*==================[typedef]================================================*/
typedef enum{
	ROTACION_0=0,
	ROTACION_90,
	ROTACION_180
}rotaciones_t;

/*==================[external data declaration]==============================*/
/** @brief Definition of constants to reference the EDU-CIAA
 */


/** Driver para controlar el HCSR4
 **
 **/


/*==================[external functions declaration]=========================*/

/** @fn void SG90Init(void)
 * @brief Inicializa el servomotor SG 90
 * @param[in] No parameter
 * @return TRUE if no error
 */
void SG90Init();
/** @fn void SG90On(void)
 * @brief Funcion que enciende los PWM outputs
 * @param[in] No parameter
 * @return TRUE if no error
 */
void SG90On();
/** @fn void SG90Rotar(rotaciones_t rotacion)
 * @brief Funcion que modifica el ancho de pulso y varia el PWM, de este modo se modifica la rotacion del servomotor
 * @param[in] rotaciones_t rotacion
 * @return TRUE if no error
 */
void SG90Rotar(rotaciones_t rotacion);

/** @fn bool SG90Deinit()
 * @brief Función que de-inicializa los pines de selección del driver
 * @param[in] no parameter
 * @return 1 (true) if no error
 */
void SG90Deinit();
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/





#endif /* #ifndef SG_90_H */

