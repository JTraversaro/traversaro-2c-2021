/* Copyright 2021
 *
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 *
 *2021
 *Traversaro Julian
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef HC05_H
#define HC05_H


/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Devices Drivers devices
 ** @{ */
/** \addtogroup HC05_H
 ** @{ */

/** @brief Bare Metal header  EDU-CIAA NXP
 **
 ** Driver para controlar un modulo bluethoot hc05
 **
 **/

/*
 * Initials     Name
 * ---------------------------
 *	TJ			Traversaro Julian
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 *
 */


/*==================[inclusions]=============================================*/
#include "bool.h"
#include "stdint.h"
#include "gpio.h"
#include "uart.h"

/*==================[macros]=================================================*/
#define COMMAND_TEST_COMMUNICATION   "AT"
#define COMMAND_GET_VERSION          "AT+VERSION?"
#define COMMAND_SET_DEFAULT          "AT+ORGL"
#define COMMAND_GET_ADDRESS          "AT+ADDR?"
#define COMMAND_GET_MOD_NAME         "AT+NAME?"
#define COMMAND_SET_MOD_NAME         "AT+NAME="   // AT+NAME=<param> ; here param is module name
#define COMMAND_GET_DEV_NAME         "AT+RNAME?"  // AT+RNAME?<param> ; param is address of bluetooth device
#define COMMAND_GET_MOD_MODE         "AT+ROLE?"
#define COMMAND_SET_MOD_MODE         "AT+ROLE="

//baud rate
#define HC05_BAUDRATE_384                38400
#define HC05_BAUDRATE_096                9600
#define HC05_BAUDRATE_115                115200

//pin
#define TX	0
#define RX	1

// roles
#define RolMaestro 1
#define RolEsclavo 0


/*==================[typedef]================================================*/
/*todos son ---> port:  RS232*/

typedef struct{
	uint32_t baud_rate;	/*!< Baud Rate:	HC05_BAUDRATE_384, 	HC05_BAUDRATE_096, HC05_BAUDRATE_115*/
	uint32_t rol;			/*!<rol: RolMaestro, RolEsclavo*/
	void *p_serial;
} hc05_config;

/*==================[external data declaration]==============================*/
/** @brief Definition of constants to reference the EDU-CIAA leds.
 */


/** \brief Definition of constants to control the EDU-CIAA leds.
 **
 **/


/*==================[external functions declaration]=========================*/

/**
 *
 */
void HC05Init(hc05_config *puerto);


/** @fn uint32_t HC05ReadStatus(uint8_t port)
 * @brief Leer el estado del puerto
 * @param[in] port Puerto que se desea leer
 */
uint32_t HC05ReadStatus();
uint32_t HC05RxReady();
uint8_t HC05ReadByte(uint8_t *dat);
void HC05SendByte(uint8_t* dat);
void HC05SendString(uint8_t *msg);/*Stops on '\0'*/
void HC05SendBuffer( const void *data, uint8_t nbytes);  /*Send n Bytes - Inline candidate...*/


/** @fn char* HC05Itoa(uint32_t val, uint8_t base)
 * @brief Conversor de entero a ASCII
 * @param[in] val Valor entero que se desea convertir
 * @param[in] base Base sobre la cual se desea realizar la conversion
 * @return Puntero al primer elemento de la cadena convertida
 */
uint8_t* HC05Itoa(uint32_t val, uint8_t base);

//uint32_t HC05DeInit(hc05_t *puerto);
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */




/*==================[end of file]============================================*/





#endif /* #ifndef HC05_H */

